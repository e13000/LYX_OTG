@echo off

:: need to change drive first
%~d0
cd %~dp0

pushd ".\utilities\"
path %cd%; %path%
popd

set downdir=.\installers
if not exist %downdir% mkdir %downdir%

set lytexdir=.
set miktexpath=http://www.ctan.org/tex-archive/systems/win32/miktex/setup/
set miktexname=miktex-portable.exe

set imagemagickpath=ftp://ftp.imagemagick.org/pub/ImageMagick/binaries/
set imagemagickname=ImageMagick-6.9.2-8-portable-Q16-x86.zip

if not exist %lytexdir%\MIKTEX goto install_miktex

echo Miktex_portable folder has already existed.
echo Select an option:
echo =============
echo -
echo 1. Install all,
echo 2. Skip  Miktex_portable's installation and Update/Install Lyx,
echo 3. Quit
echo -
set /p op=Enter 1, 2, or 3:
if "%op%"=="1" goto install_miktex
if "%op%"=="2" goto install_lyx
if "%op%"=="3" GOTO Done

:install_miktex

if exist %downdir%\%miktexname% (
echo Miktex_portable installer exists in download folder
echo.
goto extractmiktex
)

echo Downloading Miktex_portable installer...
echo.
wget -nv -N -O%downdir%\%miktexname%.tmp %miktexpath%/%miktexname%
if ErrorLevel 1 echo Error while downloading Miktex_portable installer! & pause & exit

move %downdir%\%miktexname%.tmp %downdir%\%miktexname%
if not exist %downdir%\%miktexname% echo Error while moving Miktex_portable installer & pause & exit

:extractmiktex

echo Installing Miktex_portable...
echo.
if exist %lytexdir%\MIKTEX rmdir /s /q %lytexdir%\MIKTEX
7z x -y -o%lytexdir%\MIKTEX %downdir%\%miktexname%
if ERRORLEVEL 2 echo Error while extracting Miktex_portable installer! & pause & exit

:install_lyx

call _update_lyx.bat

:install_imagemagick

if exist %downdir%\%imagemagickname% (
echo Imagemagick_portable installer exists in download folder
echo.
goto extractimagemagick
)

echo Downloading Imagemagick_portable installer...
echo.
wget -nv -N -O%downdir%\%imagemagickname%.tmp %imagemagickpath%/%imagemagickname%
if ErrorLevel 1 echo Error while downloading Imagemagick_portable installer! & pause & exit

move %downdir%\%imagemagickname%.tmp %downdir%\%imagemagickname%
if not exist %downdir%\%imagemagickname% echo Error while moving Imagemagick_portable installer & pause & exit

:extractimagemagick

echo Installing Imagemagick_portable...
echo.
if exist %lytexdir%\imagemagick rmdir /s /q %lytexdir%\imagemagick
7z x -y -o%lytexdir%\imagemagick %downdir%\%imagemagickname%
if ERRORLEVEL 2 echo Error while extracting Imagemagick_portable installer! & pause & exit

:Done
pause
exit






