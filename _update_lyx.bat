@echo off

:: need to change drive first
%~d0
cd %~dp0

pushd ".\utilities\"
path %cd%; %path%
popd

set lytexdir=.
set lyxdir=.\LyX

:: wget need the slash at the end when listing files
set lyxpath=ftp://ftp.lyx.org/pub/lyx/bin/

setlocal enabledelayedexpansion

echo Checking current LyX version...
for /f "tokens=1,2,3 delims=. " %%u in (lyxver.usb) do (
    set curver1=%%u
	set curver2=%%v
	set curver3=%%w
	set curvers=!curver1!.!curver2!.!curver3!
	echo Current LyX version is !curvers!
    echo.
)

::pause

echo Check for latest LyX version...
echo.

if exist index.html del index.html
if exist .listing del .listing
wget --no-remove-listing %lyxpath%

if not exist .listing echo Error while checking for LyX updates! & pause & exit
:: we take the last version as latest version at present
for /f "tokens=9 delims= " %%i in (.listing) do set newvers=%%i
::echo %newvers%

if exist index.html del index.html
if exist .listing del .listing

::pause

for /f "tokens=1,2,3 delims=. " %%u in ("%newvers%") do (
    set newver1=%%u
	  set newver2=%%v
	  set newver3=%%w
    echo.
	  echo Latest LyX version is !newver1!.!newver2!.!newver3!
    echo.
)

if %newver1% gtr %curver1% echo main version number was updated & goto updatelyx
if %newver1% == %curver1% if %newver2% gtr %curver2% echo minor version number was updated & goto updatelyx
if %newver1% == %curver1% if %newver2% == %curver2% if %newver3% gtr %curver3% echo revision version number was updated & goto updatelyx
if not exist %lytexdir%\LYX mkdir %lytexdir%\LYX & echo LYX is not installed & goto updatelyx

for /F %%i in ('dir /b %lytexdir%\LYX') do (
  ::LYX's folder is not empty.
  goto lastest
)

::LYX's folder is empty.
 echo LYX is not installed
 goto updatelyx


::Current LyX is in latest state
:lastest
echo Current LyX is the latest version!
goto :theEnd

:updatelyx

::pause

echo We need to update/install LyX...
echo.

::pause

set downdir=.\installers
if not exist %downdir% mkdir %downdir%

set lyxname=LyX-%newvers%-Installer-1.exe

if exist %downdir%\%lyxname% (
echo LyX installer exists in download folder
echo.
goto extractlyx
)

echo Downloading LyX installer...
echo.
wget -nv -N -O%downdir%\%lyxname%.tmp %lyxpath%%newvers%/%lyxname%
if ErrorLevel 1 echo Error while downloading LyX installer! & pause & exit

move %downdir%\%lyxname%.tmp %downdir%\%lyxname%
if not exist %downdir%\%lyxname% echo Error while moving LyX installer & pause & exit

if exist index.html del index.html
if exist .listing del .listing

::pause

:extractlyx

echo Extracting LyX installer...
echo.
if exist %downdir%\LyX rmdir /s /q %downdir%\LyX
7z x -y -o%downdir%\LyX %downdir%\%lyxname%
if ERRORLEVEL 2 echo Error while extracting LyX installer! & pause & exit

::pause

::echo.
::echo Tidying new LyX directory...
::echo.

::rmdir /s /q %downdir%\LyX\$PLUGINSDIR
:: rmdir /s /q %downdir%\LyX\$_1_

::copy /y %downdir%\LyX\bin\msvcp100.dll %downdir%\LyX\ghostscript
::copy /y %downdir%\LyX\bin\msvcr100.dll %downdir%\LyX\ghostscript

::copy /y %downdir%\LyX\bin\msvcp100.dll %downdir%\LyX\imagemagick
::copy /y %downdir%\LyX\bin\msvcr100.dll %downdir%\LyX\imagemagick

::pause

echo.
echo LyX-Installer doesn't put msvcrt in Python's directiory. We do it...
echo.

copy /y %downdir%\LyX\bin\msvcp100.dll %downdir%\LyX\Python
copy /y %downdir%\LyX\bin\msvcr100.dll %downdir%\LyX\Python

::pause

echo.
echo Removing old LyX files...
echo.

rmdir /s /q %lyxdir%\bin
rmdir /s /q %lyxdir%\ghostscript
::rmdir /s /q %lyxdir%\imagemagick
rmdir /s /q %lyxdir%\Perl
rmdir /s /q %lyxdir%\Python
rmdir /s /q %lyxdir%\Resources

::pause

echo Moving new LyX files to LyX directory...
echo.

move /y %downdir%\LyX\bin %lytexdir%\LyX
move /y %downdir%\LyX\ghostscript %lytexdir%\LyX
::move /y %downdir%\LyX\imagemagick %lytexdir%\LyX
move /y %downdir%\LyX\Perl %lytexdir%\LyX
move /y %downdir%\LyX\Python %lytexdir%\LyX
move /y %downdir%\LyX\Resources %lytexdir%\LyX

rmdir /s /q %downdir%\LyX

::pause

echo Restoring lyxrc.dist...
echo.

del %lytexdir%\LyX\Resources\lyxrc.dist
copy /y %lytexdir%\utilities\lyxrc.dist.orig %lytexdir%\LyX\Resources\lyxrc.dist

echo Reinstalling AlgoLyx...
echo.

copy /y %lytexdir%\utilities\algolyx.module %lytexdir%\LyX\Resources\layouts\algolyx.module
copy /y %lytexdir%\utilities\algolyx.sty %lytexdir%\MIKTEX\tex\latex\algolyx.sty

::pause

:: Save new version number
>lyxver.usb echo %newvers%

echo.
echo We have updated LyX from %curvers% to %newvers%.

:theEnd

endlocal
