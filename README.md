Yet Another Portable LYX for Windows.
=====================================

MIKTEX Portable + LYX + Update script from LyTex + SumatraPDF

Features
--------

+ Forward search and inverse search with SumatraPDF right off the shelf
+ Update to lastest version of Lyx just by a click (currently Lyx2.1.4)

Installation
------------

To create portable LYX from lastest release
+ [Download here](https://github.com/e13000/LYX_OTG/archive/master.zip)
+ Unzip and run _install.bat to create your portable LYX
+ [Prebuilt version](http://goo.gl/h5oGhB)

Common issues
-------------
+ Font rendering error or corrupted PDF: run TexHash
+ Image conversion error: [Install Visual C++ 2013 Redistributable Package](https://www.microsoft.com/en-us/download/details.aspx?id=40784)
+ [Remove URL in IEEEtran bibliography](http://tex.stackexchange.com/questions/45160/remove-url-in-ieeetran-bibliography)

